const ParticleSystem = require('ParticleSystem')
cc.Class({
    extends: cc.Component,

    properties: {
        particleLayer :cc.Node,
        effectLayer : cc.Node,
        particles : [cc.JsonAsset],
        controlBtn : cc.Node
    },
    start(){
        this.effectLayer.active = false;
        this.controlBtn.active = false;
        this.particleSprites = [];
    },

    onClick(e,d){
        this.effectLayer.active = true;
        this.controlBtn.active = true;

        function createNode(){
            let ps = new cc.Node().addComponent(ParticleSystem);
            this.effectLayer.addChild(ps.node)
            ps.setParticleLayer(this.particleLayer);
            ps.loadData(this.particles[d].json,true)
            ps.addEvent(ParticleSystem.Event.CREATE,this.onParticleCreate.bind(this))
            return ps;
        }

        if(d == 0){
            createNode.call(this)
        } else if(d == 1){
            let ps = createNode.call(this)
            ps.node.runAction(cc.repeatForever(cc.rotateBy(1,360)))
        } else if(d == 2){
            let ps = createNode.call(this)
            ps.node.y = 300
        } else if(d == 3){
            let ps = createNode.call(this)
            ps.node.x = -100
            ps.node.y = -300
            ps.node.rotation = -45
            ps.node.runAction(cc.repeatForever(cc.sequence(cc.rotateTo(1,45),cc.rotateTo(1,-45))))
            ps = createNode.call(this)
            ps.node.x = 100
            ps.node.rotation = 45
            ps.node.y = -300
            ps.node.runAction(cc.repeatForever(cc.sequence(cc.rotateTo(1,-45),cc.rotateTo(1,45))))
        } else if(d == 4){
            let ps = createNode.call(this)
            ps.node.y = -300
            
            ps.setLauncherData([
                {dx:-150,dy:0,dr:0},
                {dx:-75,dy:0,dr:0},
                {dx:0,dy:0,dr:0},
                {dx:75,dy:0,dr:0},
                {dx:150,dy:0,dr:0}
            ])
        }
    },
    onClickControl(e,d){
        if(d == 0){
            this.effectLayer.active = false;
            this.controlBtn.active = false;
            this.effectLayer.x =0 ;
            this.effectLayer.y = 0;
            this.effectLayer.destroyAllChildren()
        } else {
            let childrens = this.effectLayer.children
            for(var i=0; i<childrens.length; i++){
                var ps = childrens[i].getComponent(ParticleSystem)
                d == 1? ps.pause() : ps.resume()
            }
        }
    },
    onParticleCreate(target){
        cc.log('onParticleCreate:',target.detail.name)
        this.particleSprites.push(target);
        target.detail.node.on('recovery',this.onParticleRecovery,this)
    },
    onParticleRecovery(ps){
        cc.log('onParticleRecovery:',ps.node.name)
        let idx = this.particleSprites.findIndex(element=>{
            return element.detail = ps
        })
        if(idx >= 0){
            this.particleSprites.splice(idx,1);
            cc.log('移除一个粒子，剩余数量:',this.particleSprites.length)
        }
    }
});
